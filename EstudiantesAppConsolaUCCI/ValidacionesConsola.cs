﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EstudiantesAppConsolaUCCI
{
    public class ValidacionesConsola
    {
        public ConcurrentDictionary<string, string> obtenerParametrosDeConsola(string[] args)
        {
            ConcurrentDictionary<string, string> cmdArgs = null;

            if (args.Length > 0)
            {
                Regex cmdRegEx = new Regex(@"/(?<name>.+?):(?<val>.+)");                

                cmdArgs = new ConcurrentDictionary<string, string>();
                foreach (string s in args)
                {
                    Match m = cmdRegEx.Match(s);
                    if (m.Success)
                    {
                        cmdArgs.TryAdd(m.Groups[1].Value.ToLower(), m.Groups[2].Value);
                    }
                    else
                    {
                        throw new ArgumentException("No podemos continuar con la ejecución del programa, ha ingresado algun parámetro con una sintaxis incorrecta. Por favor vuelva a intentarlo verificando el parámetro a ingresar de la siguiente manera: /key:value.");
                    }
                }
            }

            return cmdArgs;
        }
    }
}
