﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace EstudiantesAppConsolaUCCI.Modelos
{
    public abstract class Estudiante : INotifyPropertyChanged
    {        
        private string apellidosValue = String.Empty;
        private string cedulaValue = String.Empty;
        private string correoInstitucionalValue = String.Empty;
        private string correoPersonalValue = String.Empty;
        private string departamentoValue = String.Empty;
        private string idAlumnoValue = String.Empty;
        private string idSGAValue = String.Empty;
        private string nombresValue = String.Empty;
        private string ubicacionValue = String.Empty;

        public string Apellidos
        {
            get { return this.apellidosValue; }
            set
            {
                if (value != this.apellidosValue)
                {
                    this.apellidosValue = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string Cedula
        {
            get { return this.cedulaValue; }
            set
            {
                if (value != this.cedulaValue)
                {
                    this.cedulaValue = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string CorreoInstitucional
        {
            get { return this.correoInstitucionalValue; }
            set
            {
                if (value != this.correoInstitucionalValue)
                {
                    this.correoInstitucionalValue = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string CorreoPersonal
        {
            get { return this.correoPersonalValue; }
            set
            {
                if (value != this.correoPersonalValue)
                {
                    this.correoPersonalValue = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string Departamento
        {
            get { return this.departamentoValue; }
            set
            {
                if (value != this.departamentoValue)
                {
                    this.departamentoValue = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string IdAlumno
        {
            get { return this.idAlumnoValue; }
            set
            {
                if (value != this.idAlumnoValue)
                {
                    this.idAlumnoValue = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string IdSGA
        {
            get { return this.idSGAValue; }
            set
            {
                if (value != this.idSGAValue)
                {
                    this.idSGAValue = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string Nombres
        {
            get { return this.nombresValue; }
            set
            {
                if (value != this.nombresValue)
                {
                    this.nombresValue = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string Ubicacion
        {
            get { return this.ubicacionValue; }
            set
            {
                if (value != this.ubicacionValue)
                {
                    this.ubicacionValue = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public override bool Equals(object obj)
        {
            try
            {
                var estudianteEsperadoAComparar = obj as ApplicationData.EstudianteControlado;

                if (estudianteEsperadoAComparar == null)
                {
                    return false;
                }

                var nombrePS = estudianteEsperadoAComparar.Nombres;
                var apellidosPS = estudianteEsperadoAComparar.Apellidos;
                var cedulaPS = estudianteEsperadoAComparar.Cedula;
                var correo = estudianteEsperadoAComparar.CorreoPersonal;
                var departamento = estudianteEsperadoAComparar.Departamento;
                var ubicacion = estudianteEsperadoAComparar.Ubicacion;

                if (this.Nombres != nombrePS)
                {
                    return false;
                }
                if (this.Apellidos != apellidosPS)
                {
                    return false;
                }
                if (this.Cedula != cedulaPS)
                {
                    return false;
                }
                if (this.CorreoPersonal != correo)
                {
                    return false;
                }
                if (this.Departamento != departamento)
                {
                    return false;
                }
                if (this.Ubicacion != ubicacion)
                {
                    return false;
                }

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
            
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public abstract IQueryable<Estudiante> ObtenerEstudiantes();

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
