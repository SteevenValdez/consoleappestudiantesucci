﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstudiantesAppConsolaUCCI
{
    internal class HelperEstudiante
    {
        const string extension = ".jpeg";
        const string ruta = @"\\FILESERVER02\public\";
        const string UChone = "CHONE";
        const string UBahia = "BAHIA";
        const string UBahiaT = "BAHÍA";
        const string UPedernales = "PEDERNALES";
        const string UCarmen = "EL CARMEN";
        const string UManta = "MANTA";

        static Random rand = new Random();


        //public static byte[] CargarImagen(string IdUsuarioAsig)
        //{
        //    string archivo = $"{ruta}{IdUsuarioAsig}{extension}";
        //    try
        //    {
        //        using (Stream BitmapStream = File.Open(archivo, FileMode.Open, FileAccess.Read))
        //        {
        //            byte[] buffer = new byte[16 * 1024];
        //            using (MemoryStream ms = new MemoryStream())
        //            {
        //                int read;
        //                while ((read = BitmapStream.Read(buffer, 0, buffer.Length)) > 0)
        //                {
        //                    ms.Write(buffer, 0, read);
        //                }
        //                return ms.ToArray();
        //            }

        //        }
        //    }
        //    catch (Exception)
        //    {

        //        return null;
        //    }
        //}

        internal static bool EsCumpleaños(DateTime fechaNacimiento)
        {
            var cumpleaños = new DateTime(DateTime.Today.Year, fechaNacimiento.Month, fechaNacimiento.Day);
            if (cumpleaños == DateTime.Today)
            {
                return true;
            }
            return false;
        }

        internal static bool EsCorreoPublico(string correoInstitucional)
        {
            if (string.IsNullOrEmpty(correoInstitucional))
            {
                return false;
            }

            if (correoInstitucional.Contains("gmail") || correoInstitucional.Contains("yahoo") || correoInstitucional.Contains("hotmail") || correoInstitucional.Contains("outlook"))
            {

                return true;
            }
            return false;

        }

        internal static string CrearContraseñaAD()
        {
            // rand = new Random();
            char[] consonantesMayusculas = { 'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z' };
            char[] vocales = { 'a', 'e', 'i', 'o', 'u' };
            char[] numeros = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            StringBuilder strB = new StringBuilder();
            strB.Append(consonantesMayusculas[rand.Next(0, 20)].ToString().ToUpper());
            strB.Append(vocales[rand.Next(0, 4)].ToString().ToLower());
            strB.Append(consonantesMayusculas[rand.Next(0, 20)].ToString().ToLower());
            strB.Append(vocales[rand.Next(0, 4)].ToString().ToLower());
            strB.Append(numeros[rand.Next(0, 9)].ToString());
            strB.Append(numeros[rand.Next(0, 9)].ToString());
            strB.Append(numeros[rand.Next(0, 9)].ToString());
            strB.Append(numeros[rand.Next(0, 9)].ToString());
            return strB.ToString();
        }



        internal static string CrearCorreoCedula(string idAlumno)
        {
            idAlumno = idAlumno.Trim();
            return $"{idAlumno}@live.uleam.edu.ec";
        }

        internal static string CrearIdAlumno(string cedula)
        {
            cedula = cedula.Trim();
            return $"e{cedula}";
        }

        internal static string CrearProperInvariantNombres(string cadena)
        {
            #region Invariant
            //cadena = Invariant(cadena);



            #endregion
            #region Espacios
            cadena = CorregirEspaciosNombres(cadena);
            #endregion
            #region Proper
            cadena = cadena.ToLower();
            cadena = ToTitleCase(cadena);
            #endregion

            return cadena;
        }

        internal static bool TieneVariant(string cadena)
        {
            if (cadena.Contains("á") || cadena.Contains("é") || cadena.Contains("í") || cadena.Contains("ó") || cadena.Contains("ú") || cadena.Contains("ñ") || cadena.Contains("Á") || cadena.Contains("É") || cadena.Contains("Í") || cadena.Contains("Ó") || cadena.Contains("Ú") || cadena.Contains("Ñ"))
            {
                return true;
            }
            return false;

        }

        internal static string CrearCorreoPersonal(string correoPersonal)
        {
            if (!string.IsNullOrEmpty(correoPersonal))
            {
                return correoPersonal.Trim();
            }
            return string.Empty;
        }

        internal static string CrearUbicacion(string ubicacion)
        {
            ubicacion = ubicacion.Trim();
            if (ubicacion.Contains(UChone))
            {
                return UChone;
            }
            else if (ubicacion.Contains(UBahia) || ubicacion.Contains(UBahiaT))
            {
                return UBahiaT;
            }
            else if (ubicacion.Contains(UCarmen))
            {
                return UCarmen;
            }
            else if (ubicacion.Contains(UPedernales))
            {
                return UPedernales;
            }
            else
            {
                return UManta;
            }
        }

        private static string Invariant(string cadena)
        {
            cadena = cadena.Replace('á', 'a');
            cadena = cadena.Replace('é', 'e');
            cadena = cadena.Replace('í', 'i');
            cadena = cadena.Replace('ó', 'o');
            cadena = cadena.Replace('ú', 'u');
            cadena = cadena.Replace('ñ', 'n');
            cadena = cadena.Replace('Á', 'A');
            cadena = cadena.Replace('É', 'E');
            cadena = cadena.Replace('Í', 'I');
            cadena = cadena.Replace('Ó', 'O');
            cadena = cadena.Replace('Ú', 'U');
            cadena = cadena.Replace('Ñ', 'N');
            return cadena;
        }

        private static string ToTitleCase(string value)
        {
            if (value == null)
                return null;
            if (value.Length == 0)
                return value;

            StringBuilder result = new StringBuilder(value);
            result[0] = char.ToUpper(result[0]);
            for (int i = 1; i < result.Length; ++i)
            {
                if (char.IsWhiteSpace(result[i - 1]))
                    result[i] = char.ToUpper(result[i]);
                else
                    result[i] = char.ToLower(result[i]);
            }
            return result.ToString();
        }

        internal static string CrearDepartamento(string departamento)
        {
            string retorno = string.Empty;
            if (departamento.Contains("CIENCIAS DE LA COMUNICACION") || departamento.Contains("CIENCIAS DE LA COMUNICACIÓN"))
            {
                retorno = "CIENCIAS DE LA COMUNICACION";
            }
            else if (departamento.Contains("CIENCIAS DE LA EDUCACION") || departamento.Contains("CIENCIAS DE LA EDUCACIÓN"))
            {
                retorno = "CIENCIAS DE LA EDUCACION";
            }
            else if (departamento.Contains("EDUCACION FISICA") || departamento.Contains("EDUCACIÓN FÍSICA"))
            {
                retorno = "EDUCACION FISICA";
            }
            else
            {
                retorno = departamento;
            }
            return retorno;
        }

        internal static string CrearContraseñaSGA(string cedula)
        {
            if (cedula.Contains("-"))
            {
                cedula = QuitarGuionCedula(cedula);
            }
            var sp = string.Empty;
            if (cedula.Length == 10)
                sp = string.Format("{0}{1}{2}{3}", cedula, cedula[3], cedula[5], cedula[7]);
            else
                sp = cedula;
            return sp;
        }

        internal static string CrearCorreoNombre(bool ini, string nombres, string apellidos)
        {

            var nombresSeparados = nombres.Split(' ');
            var apellidosSeparados = apellidos.Split(' ');

            var correo = CrearCorreo(nombresSeparados[0], apellidosSeparados[0]);
            if (ini)
            {
                var iniN = nombresSeparados.Length > 1 ? Invariant(nombresSeparados[1].Substring(0, 1).ToLower()) : string.Empty;
                var iniA = apellidosSeparados.Length > 1 ? Invariant(apellidosSeparados[1].Substring(0, 1).ToLower()) : string.Empty;

                correo = correo.Replace("@", string.Format("{0}{1}@", iniN, iniA));
            }
            return correo;

        }

        private static string CorregirEspaciosNombres(string cadena)
        {
            cadena = cadena.Trim();
            var vector = cadena.Split(' ');
            var nuevoVector = new List<string>();
            StringBuilder str = new StringBuilder();
            var nuevaCadena = string.Empty;
            foreach (var item in vector)
            {
                if (!string.IsNullOrWhiteSpace(item))
                {
                    nuevoVector.Add(item);
                }
            }
            foreach (var item in nuevoVector)
            {
                str.Append(item);
                str.Append(" ");
            }
            nuevaCadena = str.ToString().Trim();
            return nuevaCadena;
        }

        private static string CrearCorreo(string nombres, string apellidos)
        {
            return string.Format("{0}.{1}@live.uleam.edu.ec", Invariant(nombres.ToLower()), Invariant(apellidos.ToLower()));
        }

        public static string QuitarGuionCedula(string cedula)
        {
            cedula = cedula.Trim();
            if (cedula.Contains("-"))
            {
                var indice = cedula.IndexOf('-');
                cedula = cedula.Remove(indice, 1);

            }
            return cedula;
        }

        internal static bool EstaEnDepartmentoLargo(string departamento)
        {
            if (departamento.Contains("EDUCACION FISICA") || departamento.Contains("EDUCACIÓN FÍSICA") || departamento.Contains("CIENCIAS DE LA EDUCACION") || departamento.Contains("CIENCIAS DE LA COMUNICACION") || departamento.Contains("CIENCIAS DE LA EDUCACIÓN") || departamento.Contains("CIENCIAS DE LA COMUNICACIÓN"))
            {
                return true;
            }
            return false;
        }
    }
}
