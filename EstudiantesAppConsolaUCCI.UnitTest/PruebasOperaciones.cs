﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EstudiantesAppConsolaUCCI;
using EstudiantesAppConsolaUCCI.Modelos;
using EstudiantesAppConsolaUCCI.CapaAccesoDatos;
using System.Configuration;
using System.Linq;
using System.Collections.Generic;
using AutoMapper;
using AutoMapper.EquivalencyExpression;
using System.Collections.Concurrent;

namespace EstudiantesAppConsolaUCCI.UnitTest
{
    [TestClass]
    public class PruebasOperaciones
    {
        bool estaCreado = false;
        bool estaActualizado = false;

        //[TestMethod]
        //public void InsertarEst()
        //{
        //    //Obteniendo valores del App.Config                
        //    var usuarioServicio = ConfigurationSettings.AppSettings.GetValues("usuarioServicio").FirstOrDefault();
        //    var contrasenaUsuarioServicio = ConfigurationSettings.AppSettings.GetValues("contrasenaUsuarioServicio").FirstOrDefault();
        //    var direccionServicioSGA = ConfigurationSettings.AppSettings.GetValues("direccionServicioSGA").FirstOrDefault();
        //    var direccionServicioTemporal = ConfigurationSettings.AppSettings.GetValues("direccionServicioTemporal").FirstOrDefault();
        //    var keyParaBuscarPorAno = ConfigurationSettings.AppSettings.GetValues("keyParaBuscarPorAno").FirstOrDefault();

        //    //Instanciando el Acceso Remoto a Estudiantes
        //    AccesoEstudianteRemoto accesoEstudianteRemoto = new AccesoEstudianteRemoto(direccionServicioSGA, direccionServicioTemporal, usuarioServicio, contrasenaUsuarioServicio, null, keyParaBuscarPorAno);

        //    //Estudiantes Remotos del Servidor
        //    EstudianteSGA estudianteSGARemoto = new EstudianteSGA(accesoEstudianteRemoto);
        //    EstudianteTemporal estudianteTemporalRemoto = new EstudianteTemporal(accesoEstudianteRemoto);

        //    //Obtener Estudiantes Remotos del Servidor
        //    IQueryable<Estudiante> estudiantesPRemoto = estudianteSGARemoto.ObtenerEstudiantes();
        //    IQueryable<Estudiante> estudiantesControladosRemoto = estudianteTemporalRemoto.ObtenerEstudiantes();

        //    IQueryable<ApplicationData.EstudianteControlado> estudiantesControladosRemotoOriginal = accesoEstudianteRemoto.listaEstudiantesControlados;

        //    //Obtencion del Contexto del Servicio Remoto Temporal
        //    ApplicationData.ApplicationData contextoServicioTemporal = accesoEstudianteRemoto.contextoServicioTemporal;

        //    // Instanciando las Operaciones de Crear y Actualizar
        //    OperacionesEstudiante op = new OperacionesEstudiante();

        //    if (estudiantesPRemoto != null && estudiantesControladosRemoto != null)
        //    {
        //        int count1 = 0;
        //        List<Estudiante> listaEstudiantesControlados = estudiantesControladosRemoto.ToList();
        //        foreach (var estudianteSGA in estudiantesPRemoto)
        //        {
        //            estaCreado = false;
        //            estaActualizado = false;

        //            var estudianteControlado = listaEstudiantesControlados.Where(x => x.IdSGA == estudianteSGA.IdSGA).FirstOrDefault() as EstudianteTemporal;
        //            Console.WriteLine($"\n Field1: {estudianteSGA.Apellidos} | Field2: {estudianteSGA.Nombres}");

        //            if (estudianteControlado == null)
        //            {
        //                Console.WriteLine("Crear");
        //                estudianteControlado = new EstudianteTemporal();
        //                estudianteControlado.PropertyChanged += NuevoEstudianteControlado_PropertyChanged_Crear;
        //                ApplicationData.EstudianteControlado estudianteControladoRemoto = new ApplicationData.EstudianteControlado();
        //                op.CrearEstudianteSinc(estudianteSGA, estudianteControlado, estudianteControladoRemoto);
        //                contextoServicioTemporal.AddToEstudianteControlados(estudianteControladoRemoto);
        //                listaEstudiantesControlados.Add(estudianteControlado);
        //                estudianteControlado.PropertyChanged -= NuevoEstudianteControlado_PropertyChanged_Crear;

        //            }
        //            else
        //            {
        //                //Console.WriteLine("Actualizar");
        //                //estudianteControlado.PropertyChanged += EstudianteControlado_PropertyChanged_Actualizar;
        //                //ApplicationData.EstudianteControlado estudianteControladoRemoto = estudiantesControladosRemotoOriginal.Where(x => x.IdSGA == estudianteSGA.IdSGA).FirstOrDefault();
        //                //op.ActualizarEstudianteSinc(estudianteSGA, estudianteControlado, estudianteControladoRemoto);
        //                //estudianteControlado.PropertyChanged -= EstudianteControlado_PropertyChanged_Actualizar;
        //                //contextoServicioTemporal.UpdateObject(estudianteControladoRemoto);
        //            }
        //            if (estaCreado)
        //            {
        //                estudianteControlado.CreadoO365 = false;
        //                estudianteControlado.CreadoAD = false;
        //                //contextoServicioTemporal.SaveChanges();
        //                count1++;
        //            }

                    
        //        }
        //    }
        //}

    

        [TestMethod]
        public void ProbarEstudiantes()
        {
            //Obteniendo valores del App.Config                
            var usuarioServicio = ConfigurationSettings.AppSettings.GetValues("usuarioServicio").FirstOrDefault();
            var contrasenaUsuarioServicio = ConfigurationSettings.AppSettings.GetValues("contrasenaUsuarioServicio").FirstOrDefault();
            var direccionServicioSGA = ConfigurationSettings.AppSettings.GetValues("direccionServicioSGA").FirstOrDefault();
            var direccionServicioTemporal = ConfigurationSettings.AppSettings.GetValues("direccionServicioTemporal").FirstOrDefault();
            var keyParaBuscarPorAno = ConfigurationSettings.AppSettings.GetValues("keyParaBuscarPorAno").FirstOrDefault();

            string[] args = new string[] { "/ano:2016" };

            //Validando la existencia de parámetros de entrada en la consola
            ValidacionesConsola validacionesConsola = new ValidacionesConsola();
            ConcurrentDictionary<string, string> cmdArgs = validacionesConsola.obtenerParametrosDeConsola(args);

            //Instanciando el Acceso Remoto a Estudiantes
            AccesoEstudianteRemoto accesoEstudianteRemoto = new AccesoEstudianteRemoto(direccionServicioSGA, direccionServicioTemporal, usuarioServicio, contrasenaUsuarioServicio, cmdArgs, keyParaBuscarPorAno);

            //Estudiantes Remotos del Servidor
            EstudianteSGA estudianteSGARemoto = new EstudianteSGA(accesoEstudianteRemoto);
            EstudianteTemporal estudianteTemporalRemoto = new EstudianteTemporal(accesoEstudianteRemoto);

            //Obtener Estudiantes Remotos del Servidor
            IQueryable<Estudiante> estudiantesPRemoto = estudianteSGARemoto.ObtenerEstudiantes();
            IQueryable<Estudiante> estudiantesControladosRemoto = estudianteTemporalRemoto.ObtenerEstudiantes();

            IQueryable<ApplicationData.EstudianteControlado> estudiantesControladosRemotoOriginal = accesoEstudianteRemoto.listaEstudiantesControlados;

            //Obtencion del Contexto del Servicio Remoto Temporal
            ApplicationData.ApplicationData contextoServicioTemporal = accesoEstudianteRemoto.contextoServicioTemporal;

            // Instanciando las Operaciones de Crear y Actualizar
            OperacionesEstudiante op = new OperacionesEstudiante();

            if (estudiantesPRemoto != null && estudiantesControladosRemoto != null)
            {
                int countEstudiantesCreado = 0;
                int countEstudiantesActualizados = 0;
                List<Estudiante> listaEstudiantesControlados = estudiantesControladosRemoto.ToList();
                foreach (var estudianteSGA in estudiantesPRemoto)
                {
                    estaCreado = false;
                    estaActualizado = false;

                    var estudianteControlado = listaEstudiantesControlados.Where(x => x.IdSGA == estudianteSGA.IdSGA).FirstOrDefault() as EstudianteTemporal;
                    //Console.WriteLine($"\n Field1: {estudianteSGA.Apellidos} | Field2: {estudianteSGA.Nombres}");
                    ApplicationData.EstudianteControlado estudianteControladoRemoto = null;

                    if (estudianteControlado == null)
                    {
                        Console.WriteLine("Crear");
                        estudianteControlado = new EstudianteTemporal();
                        estudianteControlado.PropertyChanged += NuevoEstudianteControlado_PropertyChanged_Crear;
                        estudianteControladoRemoto = new ApplicationData.EstudianteControlado();
                        op.CrearEstudianteSinc(estudianteSGA, estudianteControlado, estudianteControladoRemoto);
                        contextoServicioTemporal.AddToEstudianteControlados(estudianteControladoRemoto);
                        listaEstudiantesControlados.Add(estudianteControlado);
                        estudianteControlado.PropertyChanged -= NuevoEstudianteControlado_PropertyChanged_Crear;
                        countEstudiantesCreado++;
                        bool estado = comprobarCamposEstudianteControlado(estudianteControladoRemoto);
                        if (!estado)
                        {
                            Console.WriteLine($"Fue en el estudiante {countEstudiantesCreado + countEstudiantesActualizados}");
                        }
                        Assert.AreEqual(true, estado);
                    }
                    else
                    {
                        Console.WriteLine("Actualizar");
                        estudianteControlado.PropertyChanged += EstudianteControlado_PropertyChanged_Actualizar;
                        estudianteControladoRemoto = estudiantesControladosRemotoOriginal.Where(x => x.IdSGA == estudianteSGA.IdSGA).FirstOrDefault();
                        op.ActualizarEstudianteSinc(estudianteSGA, estudianteControlado, estudianteControladoRemoto);
                        estudianteControlado.PropertyChanged -= EstudianteControlado_PropertyChanged_Actualizar;
                        contextoServicioTemporal.UpdateObject(estudianteControladoRemoto);
                        countEstudiantesActualizados++;
                        bool estado = estudianteSGA.Equals(estudianteControladoRemoto);
                        if (!estado)
                        {
                            Console.WriteLine($"Fue en el estudiante {countEstudiantesCreado + countEstudiantesActualizados}");
                        }
                        Assert.AreEqual(true, estado);
                    }
                    if (estaCreado || estaActualizado)
                    {
                        estudianteControlado.CreadoO365 = false;
                        estudianteControlado.CreadoAD = false;
                    }
                }
                Console.WriteLine($"Se han creado un total de {countEstudiantesCreado} estudiantes");
                Console.WriteLine($"Se han modificado un total de {countEstudiantesActualizados} estudiantes");
                Console.WriteLine($"Se han hecho un total de {countEstudiantesCreado + countEstudiantesActualizados}");
            }
        }

        private void NuevoEstudianteControlado_PropertyChanged_Crear(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            estaCreado = true;
        }

        private void EstudianteControlado_PropertyChanged_Actualizar(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            estaActualizado = true;
        }

        private Boolean comprobarCamposEstudianteControlado(EstudiantesAppConsolaUCCI.ApplicationData.EstudianteControlado estudianteControlado)
        {
            if (estudianteControlado != null)
            {
                if (estudianteControlado.IdSGA == null)
                {
                    return false;
                }
                if (estudianteControlado.Nombres == null)
                {
                    return false;
                }
                if (estudianteControlado.Apellidos == null)
                {
                    return false;
                }
                if (estudianteControlado.Cedula == null)
                {
                    return false;
                }
                if (estudianteControlado.Departamento == null)
                {
                    return false;
                }
                if (estudianteControlado.IdAlumno == null)
                {
                    return false;
                }
                if (estudianteControlado.CorreoCedula == null)
                {
                    return false;
                }
                if (estudianteControlado.CorreoNombreApellidoINI == null)
                {
                    return false;
                }
                if (estudianteControlado.CorreoNombreApellido == null)
                {
                    return false;
                }
                if (estudianteControlado.CorreoPersonal == null)
                {
                    return false;
                }
                if (estudianteControlado.ContraseñaSGA == null)
                {
                    return false;
                }
                if (estudianteControlado.ContraseñaAD == null)
                {
                    return false;
                }
                if (estudianteControlado.Ubicacion == null)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }                     
        }
    }
}
