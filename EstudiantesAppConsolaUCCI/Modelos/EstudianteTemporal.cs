﻿using EstudiantesAppConsolaUCCI.Interfaces;
using EstudiantesAppConsolaUCCI.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EstudiantesAppConsolaUCCI.CapaAccesoDatos;

namespace EstudiantesAppConsolaUCCI.Modelos
{
    public class EstudianteTemporal : Estudiante
    {
        private IEstudiantes IDALEstudiantes;
        public EstudianteTemporal()
        {

        }

        public EstudianteTemporal(IEstudiantes iobj)
        {
            IDALEstudiantes = iobj;
        }        

        public override IQueryable<Estudiante> ObtenerEstudiantes()
        {
            return IDALEstudiantes.ObtenerEstudiantesTemporal();
        }

        public bool ComandoCorreo { get; set; }
        public bool ComandoCorreoPersonal { get; set; }
        public bool ComandoUsuariosAD { get; set; }
        public bool ComandoUsuariosO365 { get; set; }
        public string ContraseñaAD { get; set; }
        public string ContraseñaO365 { get; set; }
        public string ContraseñaSGA { get; set; }
        public string CorreoCedula { get; set; }
        public string CorreoNombreApellido { get; set; }
        public string CorreoNombreApellidoINI { get; set; }
        public bool CreadoAD { get; set; }
        public bool CreadoO365 { get; set; }
        public bool EnvioCorreoBienvenida { get; set; }
        public int Id { get; set; }
        public string Mensajes { get; set; }
        public string TipoModificacion { get; set; }
    }
}
