﻿using EstudiantesAppConsolaUCCI.CapaAccesoDatos;
using EstudiantesAppConsolaUCCI.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using System.Configuration;
using System.Collections.Concurrent;

using AutoMapper;
using AutoMapper.Collection;
using AutoMapper.EquivalencyExpression;

namespace EstudiantesAppConsolaUCCI
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //Validando la existencia de parámetros de entrada en la consola
                ValidacionesConsola validacionesConsola = new ValidacionesConsola();
                ConcurrentDictionary<string, string> cmdArgs = validacionesConsola.obtenerParametrosDeConsola(args);

                //Obteniendo valores del App.Config                
                var usuarioServicio = ConfigurationSettings.AppSettings.GetValues("usuarioServicio").FirstOrDefault();
                var contrasenaUsuarioServicio = ConfigurationSettings.AppSettings.GetValues("contrasenaUsuarioServicio").FirstOrDefault();
                var direccionServicioSGA = ConfigurationSettings.AppSettings.GetValues("direccionServicioSGA").FirstOrDefault();
                var direccionServicioTemporal = ConfigurationSettings.AppSettings.GetValues("direccionServicioTemporal").FirstOrDefault();
                var keyParaBuscarPorAno = ConfigurationSettings.AppSettings.GetValues("keyParaBuscarPorAno").FirstOrDefault();

                //IUnityContainer objContainer = new UnityContainer();

                //Instanciando el Acceso Local a Estudiantes
                AccesoEstudianteLocal accesoEstudianteLocal = new AccesoEstudianteLocal();

                //Estudiantes Locales
                EstudianteSGA estudianteSGALocal = new EstudianteSGA(accesoEstudianteLocal);
                EstudianteTemporal estudianteTemporalLocal = new EstudianteTemporal(accesoEstudianteLocal);

                //Instanciando el Acceso Remoto a Estudiantes
                AccesoEstudianteRemoto accesoEstudianteRemoto = new AccesoEstudianteRemoto(direccionServicioSGA, direccionServicioTemporal, usuarioServicio, contrasenaUsuarioServicio, cmdArgs, keyParaBuscarPorAno);

                //Estudiantes Remotos del Servidor
                EstudianteSGA estudianteSGARemoto = new EstudianteSGA(accesoEstudianteRemoto);
                EstudianteTemporal estudianteTemporalRemoto = new EstudianteTemporal(accesoEstudianteRemoto);

                //Operaciones para los Estudiantes
                OperacionesEstudiante operacionesEstudiante = new OperacionesEstudiante();

                //Obtener Estudiantes Locales
                IQueryable<Estudiante> estudiantesPLocal = estudianteSGALocal.ObtenerEstudiantes();
                IQueryable<Estudiante> estudiantesControladosLocal = estudianteTemporalLocal.ObtenerEstudiantes();

                //Obtener Estudiantes Remotos del Servidor
                IQueryable<Estudiante> estudiantesPRemoto = estudianteSGARemoto.ObtenerEstudiantes();
                IQueryable<Estudiante> estudiantesControladosRemoto = estudianteTemporalRemoto.ObtenerEstudiantes();

                //IQueryable<VistaEstudiantesData.EstudianteP> estudiantesPRemotoOriginal = accesoEstudianteRemoto.listaEstudiantesSGA;
                IQueryable<ApplicationData.EstudianteControlado> estudiantesControladosRemotoOriginal = accesoEstudianteRemoto.listaEstudiantesControlados;

                //Obtencion del Contexto del Servicio Remoto Temporal
                ApplicationData.ApplicationData contextoServicioTemporal = accesoEstudianteRemoto.contextoServicioTemporal;

                //Comparar Estudiantes Local
                //operacionesEstudiante.CompararEstudiantes(estudiantesPLocal, estudiantesControladosLocal);

                //Comparar Estudiantes Remoto del Servidor
                operacionesEstudiante.CompararEstudiantes(estudiantesPRemoto, estudiantesControladosRemoto, estudiantesControladosRemotoOriginal, contextoServicioTemporal);

                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            Console.Read();          
        }
    }
}
