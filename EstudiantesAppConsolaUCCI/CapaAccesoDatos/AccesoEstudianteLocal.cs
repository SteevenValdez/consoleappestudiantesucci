﻿using EstudiantesAppConsolaUCCI.Datos;
using EstudiantesAppConsolaUCCI.Interfaces;
using EstudiantesAppConsolaUCCI.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstudiantesAppConsolaUCCI.CapaAccesoDatos
{
    class AccesoEstudianteLocal : IEstudiantes
    {
        DatosLocal datosLocales = new DatosLocal();

        public void InsertarEstudiante()
        {
            Console.WriteLine("Insertado Estudiante Localmente.");
        }
        public IQueryable<Estudiante> ObtenerEstudiantesSGA()
        {
            IQueryable<EstudianteSGA> listaEstudiantesSGA = datosLocales.obtenerEstudiantesSGA.OrderBy(x => x.Apellidos).ThenBy(x => x.Nombres);

            return listaEstudiantesSGA;
        }

        public IQueryable<Estudiante> ObtenerEstudiantesTemporal()
        {
            IQueryable<EstudianteTemporal> listaEstudiantesControlados = datosLocales.obtenerEstudiantesTemporal.OrderBy(x => x.Apellidos).ThenBy(x => x.Nombres);

            return listaEstudiantesControlados;
        }
    }
}
