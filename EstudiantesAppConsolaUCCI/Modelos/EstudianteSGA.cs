﻿using EstudiantesAppConsolaUCCI.Interfaces;
using EstudiantesAppConsolaUCCI.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstudiantesAppConsolaUCCI.Modelos
{
    public class EstudianteSGA : Estudiante
    {
        private IEstudiantes operador;
        public EstudianteSGA()
        {

        }

        public EstudianteSGA(IEstudiantes iobj)
        {
            operador = iobj;
        }

        public override IQueryable<Estudiante> ObtenerEstudiantes()
        {
            return operador.ObtenerEstudiantesSGA();
        }
    }
}
