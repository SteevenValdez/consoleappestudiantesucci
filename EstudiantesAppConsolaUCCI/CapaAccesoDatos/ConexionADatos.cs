﻿using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EstudiantesAppConsolaUCCI.CapaAccesoDatos
{
    public class ConexionADatos
    {
        public VistaEstudiantesData.VistaEstudiantesData obtenerContextoEstudiantesSGA(string direccionServicioSGA, string usuarioServicio, string contrasenaUsuarioServicio)
        {
            Uri direccionServicioSGAUri = new Uri(direccionServicioSGA, UriKind.Absolute);
            
            VistaEstudiantesData.VistaEstudiantesData contextoServicioSGA = new VistaEstudiantesData.VistaEstudiantesData(direccionServicioSGAUri);
            contextoServicioSGA.Credentials = new NetworkCredential(usuarioServicio, contrasenaUsuarioServicio);
            contextoServicioSGA.Format.UseJson();
            contextoServicioSGA.SendingRequest += (a, b) =>
            {
                ((HttpWebRequest)b.Request).Accept = "application/json";
            };

            return contextoServicioSGA;
        }
        
        public ApplicationData.ApplicationData obtenerContextoEstudiantesTemporal(string direccionServicioTemporal, string usuarioServicio, string contrasenaUsuarioServicio)
        {
            Uri direccionServicioTemporalUri = new Uri(direccionServicioTemporal, UriKind.Absolute);

            ApplicationData.ApplicationData contextoServicioTemporal = new ApplicationData.ApplicationData(direccionServicioTemporalUri);
            contextoServicioTemporal.Credentials = new NetworkCredential(usuarioServicio, contrasenaUsuarioServicio);
            contextoServicioTemporal.Format.UseJson();
            contextoServicioTemporal.SendingRequest += (a, b) =>
            {
                ((HttpWebRequest)b.Request).Accept = "application/json";
            };

            return contextoServicioTemporal;
        }
    }
}
