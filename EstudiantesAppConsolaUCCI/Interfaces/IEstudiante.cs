﻿using EstudiantesAppConsolaUCCI.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstudiantesAppConsolaUCCI.Interfaces
{
    public interface IEstudiantes
    {
        void InsertarEstudiante();

        IQueryable<Estudiante> ObtenerEstudiantesSGA();
        IQueryable<Estudiante> ObtenerEstudiantesTemporal();
    }
}
