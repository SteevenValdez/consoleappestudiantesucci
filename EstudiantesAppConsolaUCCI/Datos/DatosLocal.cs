﻿using EstudiantesAppConsolaUCCI.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstudiantesAppConsolaUCCI.Datos
{
    public class DatosLocal
    {
        public IQueryable<EstudianteSGA> obtenerEstudiantesSGA
        {
            get { return falsaListaEstudiantesSGA.AsQueryable(); }
        }

        public IQueryable<EstudianteTemporal> obtenerEstudiantesTemporal
        {
            get { return falsaListaEstudiantesTemporal.AsQueryable(); }
        }

        private List<EstudianteSGA> falsaListaEstudiantesSGA = new List<EstudianteSGA>
        {
            new EstudianteSGA { Apellidos = "CHAVEZ QUIROZ", Cedula = "1314889070", CorreoInstitucional = "e1314889070@live.uleam.edu.ec",
                CorreoPersonal = "gabytachavez_93@hotmail.com", Departamento = "FACCI", IdAlumno = "1", IdSGA = "1",
                Nombres = "GABRIELA GUADALUPE", Ubicacion = "MANTA" },
            new EstudianteSGA { Apellidos = "VALDEZ MENENDEZ", Cedula = "1310636533", CorreoInstitucional = "e131636533@live.uleam.edu.ec",
                CorreoPersonal = "bryan_valdez_menendez@hotmail.com", Departamento = "FACCI", IdAlumno = "2", IdSGA = "2",
                Nombres = "BRYAN STEEVEN", Ubicacion = "MANTA" },
            new EstudianteSGA { Apellidos = "CARRILLO MORAN", Cedula = "1332556562", CorreoInstitucional = "e1332556562@live.uleam.edu.ec",
                CorreoPersonal = "fatimaguadalupe@hotmail.com", Departamento = "FACCI", IdAlumno = "3", IdSGA = "3",
                Nombres = "FATIMA GUADALUPE", Ubicacion = "MANTA" },
            new EstudianteSGA { Apellidos = "VILELA CELORIO", Cedula = "1323365633", CorreoInstitucional = "e1323365633@live.uleam.edu.ec",
                CorreoPersonal = "josevilela@hotmail.com", Departamento = "FACCI", IdAlumno = "4", IdSGA = "4",
                Nombres = "JOSE DAVID", Ubicacion = "MANTA" },
            new EstudianteSGA { Apellidos = "ALARCON VILLAMAR", Cedula = "1319896656", CorreoInstitucional = "e1319896656@live.uleam.edu.ec",
                CorreoPersonal = "freddyalarcon@hotmail.com", Departamento = "UCCI", IdAlumno = "5", IdSGA = "5",
                Nombres = "FREDDY XAVIER", Ubicacion = "MANTA" },
        };

        private List<EstudianteTemporal> falsaListaEstudiantesTemporal = new List<EstudianteTemporal>
        {
            new EstudianteTemporal { Apellidos = "CHAVEZ QUIROZ", Cedula = "1314889070", CorreoInstitucional = "e1314889070@live.uleam.edu.ec",
                CorreoPersonal = "gabytachavez_93@hotmail.com", Departamento = "FACCI", IdAlumno = "1", IdSGA = "1",
                Nombres = "GABRIELA GUADALUPE", Ubicacion = "MANTA" },
            new EstudianteTemporal { Apellidos = "VALDEZ MENENDEZ", Cedula = "1310636533", CorreoInstitucional = "e131636533@live.uleam.edu.ec",
                CorreoPersonal = "bryan_valdez_menendez@hotmail.com", Departamento = "FACCI", IdAlumno = "2", IdSGA = "2",
                Nombres = "BRYAN STEEVEN", Ubicacion = "MANTA" },
            //ESTE CAMBIA LA CEDULA
            new EstudianteTemporal { Apellidos = "CARRILLO MORAN", Cedula = "1332556569", CorreoInstitucional = "e1332556562@live.uleam.edu.ec",
                CorreoPersonal = "fatimaguadalupe@hotmail.com", Departamento = "FACCI", IdAlumno = "3", IdSGA = "3",
                Nombres = "FATIMA GUADALUPE", Ubicacion = "MANTA" },
            //ESTE CAMBIA LA UBICACION
            new EstudianteTemporal { Apellidos = "VILELA CELORIO", Cedula = "1323365633", CorreoInstitucional = "e1323365633@live.uleam.edu.ec",
                CorreoPersonal = "josevilela@hotmail.com", Departamento = "FACCI", IdAlumno = "4", IdSGA = "4",
                Nombres = "JOSE DAVID", Ubicacion = "CHONE" },
            //EL QUINTO NO EXISTE
        };
    }
}
