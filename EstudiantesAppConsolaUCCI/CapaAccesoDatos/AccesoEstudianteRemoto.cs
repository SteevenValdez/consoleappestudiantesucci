﻿using EstudiantesAppConsolaUCCI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EstudiantesAppConsolaUCCI.Modelos;
using System.Configuration;
using System.Collections.Concurrent;
using AutoMapper;
using AutoMapper.EquivalencyExpression;

namespace EstudiantesAppConsolaUCCI.CapaAccesoDatos
{
    public class AccesoEstudianteRemoto : IEstudiantes
    {
        public VistaEstudiantesData.VistaEstudiantesData contextoServicioSGA { get; set; }
        public ApplicationData.ApplicationData contextoServicioTemporal { get; set; }
        public string anoConsulta { get; set; }
        public IQueryable<VistaEstudiantesData.EstudianteP> listaEstudiantesSGA { get; set; }
        public IQueryable<ApplicationData.EstudianteControlado> listaEstudiantesControlados { get; set; }

        public AccesoEstudianteRemoto(string direccionServicioSGA, string direccionServicioTemporal, string usuarioServicio, string contrasenaUsuarioServicio, ConcurrentDictionary<string, string> cmdArgs, string keyParaBuscarPorAno)
        {
            ConexionADatos conexionADatos = new ConexionADatos();
            contextoServicioSGA = conexionADatos.obtenerContextoEstudiantesSGA(direccionServicioSGA, usuarioServicio, contrasenaUsuarioServicio);
            contextoServicioTemporal = conexionADatos.obtenerContextoEstudiantesTemporal(direccionServicioTemporal, usuarioServicio, contrasenaUsuarioServicio);

            string anoConsultaTemporal = null;
            if (cmdArgs != null)
            {
                int anoConsultaInt = 0;
                bool existeAnoConsulta = cmdArgs.TryGetValue(keyParaBuscarPorAno, out anoConsultaTemporal);
                if (int.TryParse(anoConsultaTemporal, out anoConsultaInt))
                {
                    anoConsulta = anoConsultaTemporal;
                }
                else
                {
                    anoConsulta = null;
                }
            }
        }

        public void InsertarEstudiante()
        {
            Console.WriteLine("Insertado Estudiante en el Servidor.");
        }                

        IQueryable<Estudiante> IEstudiantes.ObtenerEstudiantesSGA()
        {
            //IQueryable<VistaEstudiantesData.EstudianteP> listaEstudiantesSGA = null;
            listaEstudiantesSGA = null;
            if (anoConsulta != null)
            {
                listaEstudiantesSGA = contextoServicioSGA.EstudiantePS.Where(x => x.IdSGA.Substring(0, 4) == anoConsulta).OrderBy(x => x.Apellidos).ThenBy(x => x.Nombres);
            }
            else
            {
                listaEstudiantesSGA = contextoServicioSGA.EstudiantePS.OrderBy(x => x.Apellidos).ThenBy(x => x.Nombres);
            }

            Mapper.Initialize(x =>
            {
                x.AddCollectionMappers();
                x.CreateMap<VistaEstudiantesData.EstudianteP, EstudianteSGA>();
            });

            List<VistaEstudiantesData.EstudianteP> listaEstudiantesSGAToList = listaEstudiantesSGA.ToList();

            listaEstudiantesSGA = listaEstudiantesSGAToList.AsQueryable();

            var listaMapeadaEstudiantesSGA = Mapper.Map<List<EstudianteSGA>>(listaEstudiantesSGA).AsQueryable();

            return listaMapeadaEstudiantesSGA;
        }

        IQueryable<Estudiante> IEstudiantes.ObtenerEstudiantesTemporal()
        {
            //IQueryable<ApplicationData.EstudianteControlado> listaEstudiantesControlados = null;
            listaEstudiantesControlados = null;
            if (anoConsulta != null)
            {
                listaEstudiantesControlados = contextoServicioTemporal.EstudianteControlados.Where(x => x.IdSGA.Substring(0, 4) == anoConsulta).OrderBy(x => x.Apellidos).ThenBy(x => x.Nombres);
            }
            else
            {
                listaEstudiantesControlados = contextoServicioTemporal.EstudianteControlados.OrderBy(x => x.Apellidos).ThenBy(x => x.Nombres);
            }

            Mapper.Initialize(x =>
            {
                x.AddCollectionMappers();
                x.CreateMap<ApplicationData.EstudianteControlado, EstudianteTemporal>();
            });

            List<ApplicationData.EstudianteControlado> listaEstudiantesControladosToList = listaEstudiantesControlados.ToList();

            listaEstudiantesControlados = listaEstudiantesControladosToList.AsQueryable();

            var listaMapeadaEstudiantesControlados = Mapper.Map<List<EstudianteTemporal>>(listaEstudiantesControlados).AsQueryable();

            return listaMapeadaEstudiantesControlados;
        }
    }
}
