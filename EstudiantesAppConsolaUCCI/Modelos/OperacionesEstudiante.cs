﻿using AutoMapper;
using AutoMapper.EquivalencyExpression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstudiantesAppConsolaUCCI.Modelos
{
    public class OperacionesEstudiante
    {
        bool estaCreado = false;
        bool estaActualizado = false;

        public void CompararEstudiantes(IQueryable<Estudiante> estudiantesP, IQueryable<Estudiante> estudiantesControlados)
        {
            if (estudiantesP != null && estudiantesControlados != null)
            {
                int count2 = 0;
                List<Estudiante> listaEstudiantesControlados = estudiantesControlados.ToList();
                foreach (var estudianteSGA in estudiantesP)
                {
                    estaCreado = false;
                    estaActualizado = false;

                    var estudianteControlado = listaEstudiantesControlados.Where(x => x.IdSGA == estudianteSGA.IdSGA).FirstOrDefault() as EstudianteTemporal;
                    Console.WriteLine($"\n Field1: {estudianteSGA.Apellidos} | Field2: {estudianteSGA.Nombres}");

                    if (estudianteControlado == null)
                    {
                        Console.WriteLine("Crear");
                        estudianteControlado = new EstudianteTemporal();
                        estudianteControlado.PropertyChanged += NuevoEstudianteControlado_PropertyChanged_Crear;
                        CrearEstudianteSinc(estudianteSGA, estudianteControlado);
                        listaEstudiantesControlados.Add(estudianteControlado);
                        estudianteControlado.PropertyChanged -= NuevoEstudianteControlado_PropertyChanged_Crear;
                    }
                    else
                    {
                        Console.WriteLine("Actualizar");
                        estudianteControlado.PropertyChanged += EstudianteControlado_PropertyChanged_Actualizar;
                        ActualizarEstudianteSinc(estudianteSGA, estudianteControlado);
                        estudianteControlado.PropertyChanged -= EstudianteControlado_PropertyChanged_Actualizar;
                    }
                    if (estaCreado || estaActualizado)
                    {
                        estudianteControlado.CreadoO365 = false;
                        estudianteControlado.CreadoAD = false;
                    }

                    count2++;
                }
                Console.WriteLine($"El estudiantesSGA tiene {count2} registros");
            }            
        }

        public void CompararEstudiantes(IQueryable<Estudiante> estudiantesP, IQueryable<Estudiante> estudiantesControlados,
            IQueryable<ApplicationData.EstudianteControlado> estudiantesControladosRemotoOriginal, ApplicationData.ApplicationData contextoServicioTemporal)
        {            
            if (estudiantesP != null && estudiantesControlados != null)
            {
                int count2 = 0;
                List<Estudiante> listaEstudiantesControlados = estudiantesControlados.ToList();
                foreach (var estudianteSGA in estudiantesP)
                {
                    estaCreado = false;
                    estaActualizado = false;

                    var estudianteControlado = listaEstudiantesControlados.Where(x => x.IdSGA == estudianteSGA.IdSGA).FirstOrDefault() as EstudianteTemporal;
                    Console.WriteLine($"\n Field1: {estudianteSGA.Apellidos} | Field2: {estudianteSGA.Nombres}");
                    ApplicationData.EstudianteControlado estudianteControladoRemoto = null;

                    if (estudianteControlado == null)
                    {
                        Console.WriteLine("Crear");
                        estudianteControlado = new EstudianteTemporal();
                        estudianteControlado.PropertyChanged += NuevoEstudianteControlado_PropertyChanged_Crear;
                        estudianteControladoRemoto = new ApplicationData.EstudianteControlado();
                        CrearEstudianteSinc(estudianteSGA, estudianteControlado, estudianteControladoRemoto);
                        contextoServicioTemporal.AddToEstudianteControlados(estudianteControladoRemoto);
                        listaEstudiantesControlados.Add(estudianteControlado);
                        estudianteControlado.PropertyChanged -= NuevoEstudianteControlado_PropertyChanged_Crear;
                            
                    }
                    else
                    {
                        Console.WriteLine("Actualizar");
                        estudianteControlado.PropertyChanged += EstudianteControlado_PropertyChanged_Actualizar;
                        estudianteControladoRemoto = estudiantesControladosRemotoOriginal.Where(x => x.IdSGA == estudianteSGA.IdSGA).FirstOrDefault();
                        ActualizarEstudianteSinc(estudianteSGA, estudianteControlado, estudianteControladoRemoto);
                        estudianteControlado.PropertyChanged -= EstudianteControlado_PropertyChanged_Actualizar;
                        contextoServicioTemporal.UpdateObject(estudianteControladoRemoto);
                    }
                    if (estaCreado || estaActualizado)
                    {
                        estudianteControlado.CreadoO365 = false;
                        estudianteControlado.CreadoAD = false;
                        //contextoServicioTemporal.SaveChanges();
                    }

                    count2++;
                }
                Console.WriteLine($"El estudiantesSGA tiene {count2} registros");
            }            
        }

        private void NuevoEstudianteControlado_PropertyChanged_Crear(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            estaCreado = true;
        }

        private void EstudianteControlado_PropertyChanged_Actualizar(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            estaActualizado = true;
        }

        public void CrearEstudianteSinc(Estudiante estudianteSGA, EstudianteTemporal estudianteControlado)
        {
            estudianteControlado.IdSGA = estudianteSGA.IdSGA;
            estudianteControlado.Nombres = HelperEstudiante.CrearProperInvariantNombres(estudianteSGA.Nombres);
            estudianteControlado.Apellidos = HelperEstudiante.CrearProperInvariantNombres(estudianteSGA.Apellidos);
            estudianteControlado.Cedula = HelperEstudiante.QuitarGuionCedula(estudianteSGA.Cedula);
            estudianteControlado.Departamento = HelperEstudiante.CrearDepartamento(estudianteSGA.Departamento);
            estudianteControlado.IdAlumno = HelperEstudiante.CrearIdAlumno(estudianteControlado.Cedula);
            estudianteControlado.CorreoCedula = HelperEstudiante.CrearCorreoCedula(estudianteControlado.IdAlumno);
            estudianteControlado.CorreoNombreApellidoINI = HelperEstudiante.CrearCorreoNombre(true, estudianteControlado.Nombres, estudianteControlado.Apellidos);
            estudianteControlado.CorreoNombreApellido = HelperEstudiante.CrearCorreoNombre(false, estudianteControlado.Nombres, estudianteControlado.Apellidos);
            estudianteControlado.CorreoPersonal = HelperEstudiante.CrearCorreoPersonal(estudianteSGA.CorreoPersonal);
            estudianteControlado.ContraseñaSGA = HelperEstudiante.CrearContraseñaSGA(estudianteControlado.Cedula);
            estudianteControlado.ContraseñaAD = HelperEstudiante.CrearContraseñaAD();
            estudianteControlado.Ubicacion = HelperEstudiante.CrearUbicacion(estudianteSGA.Ubicacion);
        }

        public void CrearEstudianteSinc(Estudiante estudianteSGA, EstudianteTemporal estudianteControlado, ApplicationData.EstudianteControlado nuevoEstudianteControlado)
        {            
            estudianteControlado.IdSGA = estudianteSGA.IdSGA;
            estudianteControlado.Nombres = HelperEstudiante.CrearProperInvariantNombres(estudianteSGA.Nombres);
            estudianteControlado.Apellidos = HelperEstudiante.CrearProperInvariantNombres(estudianteSGA.Apellidos);
            estudianteControlado.Cedula = HelperEstudiante.QuitarGuionCedula(estudianteSGA.Cedula);
            estudianteControlado.Departamento = HelperEstudiante.CrearDepartamento(estudianteSGA.Departamento);
            estudianteControlado.IdAlumno = HelperEstudiante.CrearIdAlumno(estudianteControlado.Cedula);
            estudianteControlado.CorreoCedula = HelperEstudiante.CrearCorreoCedula(estudianteControlado.IdAlumno);
            estudianteControlado.CorreoNombreApellidoINI = HelperEstudiante.CrearCorreoNombre(true, estudianteControlado.Nombres, estudianteControlado.Apellidos);
            estudianteControlado.CorreoNombreApellido = HelperEstudiante.CrearCorreoNombre(false, estudianteControlado.Nombres, estudianteControlado.Apellidos);
            estudianteControlado.CorreoPersonal = HelperEstudiante.CrearCorreoPersonal(estudianteSGA.CorreoPersonal);
            estudianteControlado.ContraseñaSGA = HelperEstudiante.CrearContraseñaSGA(estudianteControlado.Cedula);
            estudianteControlado.ContraseñaAD = HelperEstudiante.CrearContraseñaAD();
            estudianteControlado.Ubicacion = HelperEstudiante.CrearUbicacion(estudianteSGA.Ubicacion);

            nuevoEstudianteControlado.IdSGA = estudianteControlado.IdSGA;
            nuevoEstudianteControlado.Nombres = estudianteControlado.Nombres;
            nuevoEstudianteControlado.Apellidos = estudianteControlado.Apellidos;
            nuevoEstudianteControlado.Cedula = estudianteControlado.Cedula;
            nuevoEstudianteControlado.Departamento = estudianteControlado.Departamento;
            nuevoEstudianteControlado.IdAlumno = estudianteControlado.IdAlumno;
            nuevoEstudianteControlado.CorreoCedula = estudianteControlado.CorreoCedula;
            nuevoEstudianteControlado.CorreoNombreApellidoINI = estudianteControlado.CorreoNombreApellidoINI;
            nuevoEstudianteControlado.CorreoNombreApellido = estudianteControlado.CorreoNombreApellido;
            nuevoEstudianteControlado.CorreoPersonal = estudianteControlado.CorreoPersonal;
            nuevoEstudianteControlado.ContraseñaSGA = estudianteControlado.ContraseñaSGA;
            nuevoEstudianteControlado.ContraseñaAD = estudianteControlado.ContraseñaAD;
            nuevoEstudianteControlado.Ubicacion = estudianteControlado.Ubicacion;
        }

        public void ActualizarEstudianteSinc(Estudiante estudianteSGA, Estudiante estudianteControlado)
        {
            var nombrePS = estudianteSGA.Nombres;
            var apellidosPS = estudianteSGA.Apellidos;
            var cedulaPS = estudianteSGA.Cedula;
            var correo = estudianteSGA.CorreoPersonal;
            var departamento = estudianteSGA.Departamento;
            var ubicacion = estudianteSGA.Ubicacion;

            if (estudianteControlado.Nombres != nombrePS)
            {
                estudianteControlado.Nombres = nombrePS;
            }
            if (estudianteControlado.Apellidos != apellidosPS)
            {
                estudianteControlado.Apellidos = apellidosPS;
            }
            if (estudianteControlado.Cedula != cedulaPS)
            {
                estudianteControlado.Cedula = cedulaPS;                
            }
            if (estudianteControlado.CorreoPersonal != correo)
            {
                estudianteControlado.CorreoPersonal = correo;
            }
            if (estudianteControlado.Departamento != departamento)
            {
                estudianteControlado.Departamento = departamento;
            }
            if (estudianteControlado.Ubicacion != ubicacion)
            {
                estudianteControlado.Ubicacion = ubicacion;
            }  
        }

        public void ActualizarEstudianteSinc(Estudiante estudianteSGA, Estudiante estudianteControlado, ApplicationData.EstudianteControlado estudianteControladoRemoto)
        {
            var nombrePS = estudianteSGA.Nombres;
            var apellidosPS = estudianteSGA.Apellidos;
            var cedulaPS = estudianteSGA.Cedula;
            var correo = estudianteSGA.CorreoPersonal;
            var departamento = estudianteSGA.Departamento;
            var ubicacion = estudianteSGA.Ubicacion;

            if (estudianteControlado.Nombres != nombrePS)
            {
                estudianteControlado.Nombres = nombrePS;
                estudianteControladoRemoto.Nombres = nombrePS;
            }
            if (estudianteControlado.Apellidos != apellidosPS)
            {
                estudianteControlado.Apellidos = apellidosPS;
                estudianteControladoRemoto.Apellidos = apellidosPS;
            }
            if (estudianteControlado.Cedula != cedulaPS)
            {
                estudianteControlado.Cedula = cedulaPS;
                estudianteControladoRemoto.Cedula = cedulaPS;
            }
            if (estudianteControlado.CorreoPersonal != correo)
            {
                estudianteControlado.CorreoPersonal = correo;
                estudianteControladoRemoto.CorreoPersonal = correo;
            }
            if (estudianteControlado.Departamento != departamento)
            {
                estudianteControlado.Departamento = departamento;
                estudianteControladoRemoto.Departamento = departamento;
            }
            if (estudianteControlado.Ubicacion != ubicacion)
            {
                estudianteControlado.Ubicacion = ubicacion;
                estudianteControladoRemoto.Ubicacion = ubicacion;
            }
        }
    }
}
